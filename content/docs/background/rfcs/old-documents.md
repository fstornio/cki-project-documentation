---
title: "Old Request for Comments"
linkTitle: Old RFCs
description: |
    Old Request for Comments hosted on Google Drive (Red Hat Internal)
---

- 2018-07-17: [Database schema planning for sktm 2.0] (Major Hayden)
- 2018-08-27: [Taking Kernel CI Upstream] (Major Hayden)
- 2018-10-09: [Skt – KPET interface] (Nikolai Kondrashov)
- 2018-10-10: [Kernel compile pipeline] (Major Hayden)
- 2018-10-16: [Kernel CI Ideals] (Major Hayden)
- 2019-07-17: [CKI Data Warehouse Proposal] (Major Hayden)
- 2019-10-07: [CKI Reporting Overhaul] (Nikolai Kondrashov)
- 2019-10-11: [Data Warehouse Data Storage Spec] (Michael Hofmann)
- 2020-03-31: [Generating KCIDB data in the pipeline] (Veronika Kabátová)
- 2020-03-31: [High level overview of the CKI data flow] (Veronika Kabátová)
- 2020-10-01: [CKI and Patchwork] (Don Howard)

[Database schema planning for sktm 2.0]: https://docs.google.com/document/d/1-fZ9Zctb-7Rcp2pZw_ODiuxMWFa-nNkpfr6vRoVJaSk
[Taking Kernel CI Upstream]: https://docs.google.com/document/d/1KIJicFKh1d1a1rLHT2v7Rzkl2u_-YE6dOvSQwaRQbnM
[Skt – KPET interface]: https://docs.google.com/document/d/11w5gOuKUsrNuTFCuMWngnfBUZLQLUjtppE3wtC5UY3A
[Kernel compile pipeline]: https://docs.google.com/document/d/1sM6popQdxSH3nsRzhKUY52yFFRjdWJWLqhr_16zXFoc
[Kernel CI Ideals]: https://docs.google.com/document/d/1AIWpBZxsp5o2TqdRLIjRnoUWaIveBVKkSxkHFrFxeyc
[CKI Data Warehouse Proposal]: https://docs.google.com/document/d/1SCMps5FCTmic5RMwAHusPOv4RZY5ocgYPDCcCtlK1TA
[CKI Reporting Overhaul]: https://docs.google.com/document/d/1LPsxwcAaaDaBj90HujfVSJaj7j8QlJzl6gGQ6dk_h2A
[Data Warehouse Data Storage Spec]: https://docs.google.com/document/d/1SJDZvuKCot5C9S0PHSxf6Z5m6MvYnYANBoN_Cs6ccN8
[Generating KCIDB data in the pipeline]: https://docs.google.com/document/d/18dG4yhaz6my2E5aU_hvDPbQqzJOrSEdxY6qT4paYeTU
[High level overview of the CKI data flow]: https://docs.google.com/document/d/1IavJ6Ftq2uqV-e8x6PDR4o7-MpzWX-x3US5cz53SFkw
[CKI and Patchwork]: https://docs.google.com/document/d/1um0HIio92TGfiabmLAj9F80V4ny3_dijWkx9pNTu3P8

<!-- vi: set spell spelllang=en: -->
