---
title: Refreshing tokens
description: >-
  How to renew/rotate time-limited secret tokens
aliases: [/l/refresh-tokens]
---

## Problem

You want to renew time-limited secret tokens because their expiration date is
looming.

### Steps

1. In the secrets file, copy the complete secret that is going to expire to a
   new name.

2. Remove the secret data by deleting `.data`, only keeping the token meta
   information in `.meta`.

3. Create the new token and update secret data and token meta information:

   - For token types where creation is supported by the [credential-manager]:

     1. Remove all meta information fields that are not marked `required` in
        the `create` column of the appropriate table in the
        [credential-manager] documentation.

     2. Create the new token as explained in the [credential-manager]
        documentation. This will fill in the new secret data in `.data`, and
        also update the token meta information in `meta`.

   - For all other token types:

     1. Create the new token manually by whatever means appropriate.

     2. Update the meta information fields accordingly by editing the YAML file.

     3. Encrypt the token secret as explained in the [secrets-helper]
        documentation.

4. Update all users of the token to the name of the new secret, and submit the
   MR.

[credential-manager]: https://cki-project.org/l/credential-manager-docs
[secrets-helper]: https://cki-project.org/l/secrets-helper-docs
