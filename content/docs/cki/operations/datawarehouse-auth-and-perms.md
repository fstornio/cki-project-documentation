---
title: DataWarehouse Authorization and Permission
description: How DataWarehouse hides internal data and handles user access
aliases: [/l/datawarehouse-auth-and-perms]
---

{{% alert title="TL;DR" color="info" %}}

- There are 3 visibility (`Policy`) levels: public, internal, retrigger;
- Objects (`Issue` and `KCIDBCheckout` + children) visibility is defined by the `Policy` they are
  created with;
- Grant permissions to Users using DW Groups; preferably using [LDAP group
  links][admin-ldapgrouplink];
- DW Groups required to triage internal objects: "Triagers", "policy_public_write",
  "policy_internal_read", "policy_internal_write".
- CKI team members can be assigned to the DW Group "CKI Team", or be enabled as a superuser.

{{% /alert %}}

DataWarehouse is powered by **Django**, which comes with a built-in [permissions][django-permission]
system to manage users and group access to read and write operations per table, e.g. who can submit
results, create issues and remove occurrences. However, it doesn't provide a way to handle
object-level permissions, what in our case means basically which users should be able to interact
with internal objects.

**Django Rest Framework** offers a framework to design [custom permissions], which we've used to
define 3 levels of visibility in our "[PolicyAuthorizationPermission][policy-permission]": `public`,
`internal` and `retrigger`. In summary, we've created the `Policy` table to connect database objects
to user groups, i.e objects (`Issue` and `KCIDBCheckout`) point to one of 3 policies, each with a
`read_group` and `write_group`:

1. **public**: Anyone can read these objects (`read_group=None`). Users in the `policy_public_write`
   group can update objects with this policy.
2. **internal**: Users in the `policy_internal_read` group can access objects with this policy.
   Users in the `policy_internal_write` group can update objects with this policy.
3. **retrigger**: Users in the `policy_retrigger_rw` group can access and update objects with this
   policy. This data has a debug nature, and is submitted by the cki-ci-bot's pipelines, triggered
   when testing merge requests. This policy is used only by service-accounts, however superusers
   have the same privileges.

## LDAP/Rover Group Sync

We have an hourly cronjob ([`UpdateLdapGroupMembers`][dw-cronjobs]) to keep DW groups synchronized
to LDAP/Rover groups, which is the preferable way to handle permissions. It works as a simple 1:1
relationship between a DW Group and a LDAP query, managed by
[admin/LDAP-group-link][admin-ldapgrouplink].

## Triaging permissions for QE Teams

To enable an user to triage issues, you need to assign them to the `Triagers` and `public-write`
groups. To triage internal data, they would also need `internal-read` and `internal-write`.

{{% alert color="info" %}}

1. `Triagers`: Grant permission to add and delete issue occurrences, among other things.
2. `policy_public_write`: Grant permission to create, update and delete public data.
3. `policy_internal_read`: Grant permission to list and read public data.
4. `policy_internal_write`: Grant permission to create, update and delete internal data.

{{% /alert %}}

In the past, that meant adding them to the following Rover groups, which might still be the case for
some users:

| Rover Group                        | DW Groups                                    |
| ---------------------------------- | -------------------------------------------- |
| [cki-datawarehouse-public-write]   | `public-write`                               |
| [cki-datawarehouse-internal-write] | `internal-write`                             |
| [linux-eng-pe]                     | `internal-read`                              |
| [bugzilla-redhat]                  | `internal-read`                              |
| [cki-kernel-tests-reviewers]       | `public-write`, `internal-write`, `Triagers` |

Nowadays, we have been using a team-based permission management, granting triaging permissions to
all members of interested QE teams, creating [LDAP group link][admin-ldapgrouplink] for the four
groups mentioned above. Currently, there are 4 teams enabled for this:

- [Kernel QE (kernel-qe)](https://rover.redhat.com/groups/group/se-kernel)
- [Kernel SE (se-kernel)](https://rover.redhat.com/groups/group/se-kernel)
- [Linux QE (linux-qe)](https://rover.redhat.com/groups/group/se-kernel)
- [Network QE (network-qe)](https://rover.redhat.com/groups/group/se-kernel)

{{% alert title="Enabling triaging for a new QE Team" color="info" %}}

To grant permission to a new team:

1. get the LDAP query from their Rover page
2. [add 4 records of LDAP group link][add-ldap-link], one for each DW Group: `Triagers`,
   `public-write`, `internal-write`, `internal-write`.

{{% /alert %}}

## API authentication and permission

In short, we are using DRF token authentication, which should provide users with the same
permissions they have when accessing the website in the browser. There's a little bit of additional
information about it in [DataWarehouse API permissions documentation][dw-api-perms], but we are
[planning to move to OIDC authentication][oidc].

[admin-ldapgrouplink]: https://datawarehouse.cki-project.org/admin/datawarehouse/ldapgrouplink/
[django-permission]: https://docs.djangoproject.com/en/5.0/topics/auth/default/#permissions-and-authorization
[custom permissions]: https://www.django-rest-framework.org/api-guide/permissions/#custom-permissions
[policy-permission]: https://gitlab.com/cki-project/datawarehouse/-/blob/main/datawarehouse/api/permissions.py
[dw-cronjobs]: https://gitlab.com/cki-project/datawarehouse/-/blob/main/datawarehouse/cron/jobs.py
[add-ldap-link]: https://datawarehouse.cki-project.org/admin/datawarehouse/ldapgrouplink/add/
[cki-datawarehouse-public-write]: https://rover.redhat.com/groups/group/cki-datawarehouse-public-write
[cki-datawarehouse-internal-write]: https://rover.redhat.com/groups/group/cki-datawarehouse-internal-write
[linux-eng-pe]: https://rover.redhat.com/groups/group/linux-eng-pe
[bugzilla-redhat]: https://rover.redhat.com/groups/group/bugzilla-redhat
[cki-kernel-tests-reviewers]: https://rover.redhat.com/groups/group/cki-kernel-tests-reviewers
[dw-api-perms]: https://cki-project.gitlab.io/datawarehouse/permissions.html
[oidc]: https://gitlab.com/cki-project/datawarehouse/-/issues/398
