---
baseURL: https://cki-project.org

title: CKI Project

enableRobotsTXT: true

# Hugo allows theme composition (and inheritance). The precedence is from left
# to right.
theme: [github.com/google/docsy, github.com/google/docsy/dependencies]

disableKinds: [taxonomy, taxonomyTerm]

# Highlighting config
pygmentsCodeFences: true
pygmentsUseClasses: false
# Use the new Chroma Go highlighter in Hugo.
pygmentsUseClassic: false
# pygmentsOptions: linenos=table
# See https://help.farbox.com/pygments.html
pygmentsStyle: tango

module:
  imports:
    - path: gitlab.com/cki-project/cki-lib
      mounts:
        - {source: documentation, target: content/docs/background/cki-lib}
    - path: gitlab.com/cki-project/cki-tools
      mounts:
        - {source: documentation, target: content/docs/background/cki-tools}
    - path: gitlab.com/cki-project/pipeline-data
      mounts:
        - {source: documentation, target: content/docs/background/kernel-testing}

# Image processing configuration.
imaging:
  resampleFilter: CatmullRom
  quality: 75
  anchor: smart

markup:
  goldmark:
    renderer:
      unsafe: true
  highlight:
    style: tango

params:
  companionHost: https://documentation.internal.cki-project.org
  copyright: The CKI Project Authors
  # privacy_policy: https://policies.google.com/privacy

  # First one is picked as the Twitter card image if not set on page.
  # images: [images/project-illustration.png]

  # Enable Lunr.js offline search
  offlineSearch: true

  # Custom variables
  # Theme params
  time_format_blog: 2006.01.02
  time_format_default: 2006.01.02

  # User interface configuration
  ui:
    # Show the side bar menu in its compact state.
    sidebar_menu_compact: true
    # Indicate that there are subpages
    sidebar_menu_foldable: true
    # Disable the About link in the site footer
    footer_about_disable: true

  home:
    - title: Kernel developers
      url: /docs/user_docs/
      url_text: Start here if you are a kernel developer
      icon: fas fa-microchip
    - title: Test maintainers
      url: /docs/test-maintainers/
      url_text: Start here if you are a test maintainer
      icon: fas fa-flask
    - title: CKI contributors
      url: /docs/cki/
      url_text: Start here if you want to contribute to CKI itself
      icon: fab fa-gitlab

  links:
    left:
      title: General information
      links:
        - name: Test results
          url: https://datawarehouse.cki-project.org/
          desc: DataWarehouse
        - name: Code repositories
          url: https://gitlab.com/cki-project
          desc: gitlab.com/cki-project
        - name: Issue tracker
          url: https://gitlab.com/groups/cki-project/-/issues
          desc: gitlab.com/cki-project
    right:
      title: Contact
      links:
        - name: Report documentation issue
          url: https://gitlab.com/cki-project/documentation/-/issues/new
          icon: fa-solid fa-book
        - name: Report project issue
          url: https://gitlab.com/cki-project/infrastructure/-/issues/new
          icon: fa-solid fa-bug
        - name: Mailing list
          url: mailto:cki-project@redhat.com
          desc: cki-project@redhat.com

menu:
  main:
    - name: Test results
      post: ' <i class="fa-solid fa-up-right-from-square"></i>'
      url: https://datawarehouse.cki-project.org/
      weight: 20
    - name: Code
      post: ' <i class="fa-solid fa-up-right-from-square"></i>'
      url: https://gitlab.com/cki-project/
      weight: 30
