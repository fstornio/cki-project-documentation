module gitlab.com/cki-project/documentation

go 1.19

require (
	github.com/google/docsy v0.7.1 // indirect
	gitlab.com/cki-project/cki-lib v1.0.0 // indirect
	gitlab.com/cki-project/cki-tools v1.0.0 // indirect
	gitlab.com/cki-project/pipeline-data v1.0.0 // indirect
)
