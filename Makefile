.PHONY: default setup link-check lint serve

SHELL := /bin/bash

default: serve

# The "documentation" volume expects the public documentation repository
# to have been cloned with its default name, and it's used by the internal docs
path_to_public_docs := ../documentation

podman:
	podman run \
	    --pull=newer \
	    --rm \
	    --interactive \
	    --tty \
	    --volume .:/data:Z \
	    --volume $(path_to_public_docs):/documentation:Z \
	    --workdir /data \
	    --network host \
	    quay.io/cki/hugo-docs:production

setup:
	npm i --no-package-lock

link-check:
	./check-links.sh

lint:
	markdownlint content/

# Serve until "Ctrl+C", then clean
serve:
	trap '$(MAKE) clean; exit 0' SIGINT; hugo serve

# Remove lock files
clean:
	rm -f go.sum > /dev/null
	rm -f .hugo_build.lock > /dev/null
	git checkout HEAD go.mod
